###  Solve the problem of communication between components
* 为了解决前端组件间通信问题
* 安装 install
```
npm i verb.js  -S
```
* Instructions for use
```
const bus=require('verb.js').Event;
//example 发布订阅一个send事件
bus.on('send',callback)
//订阅send事件
bus.emit('send',arguments)
//发布send事件
bus.remove('send',callback)
//移除订阅的事件
